# Journal Template

An environment for IEEE journal papers. 

## Initial Setup

Source documents : found on [IEEE Article Templates - IEEE Author Center Journals](https://journals.ieeeauthorcenter.ieee.org/create-your-ieee-journal-article/authoring-tools-and-templates/tools-for-ieee-authors/ieee-article-templates/)

- [IEEE-Transactions-LaTeX2e-templates-and-instructions.zip (ZIP, 529 KB)](https://template-selector.ieee.org/api/ieee-template-selector/template/184/download) Updated Dec. 2023

Please read the provided `New_IEEEtran_how-to.pdf` in the zip archive. 
More information (latex stuff) are available in the [conference template](https://gitlab.telecom-paris.fr/c2s/ieee/conference-template). 

