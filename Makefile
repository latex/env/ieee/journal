# Makefile to build conference paper
# based on 
# https://gitlab.telecom-paris.fr/latex/beamer-tpt-ng/-/blob/master/Makefile

TOP ?= bare_jrnl_new_sample4.pdf
TOP_NAME = $(patsubst %.pdf,%,$(TOP))

STYS       = $(wildcard texinputs/*.sty)
BIBS	   = $(wildcard bibinputs/*.bib)
BSTS	   = $(wildcard bibinputs/*.bst)

FIGS       = $(wildcard figs/*.fig)
FIGS_PDF   = $(patsubst %.fig,%.pdftex,$(FIGS))
FIGS_PDF_T = $(patsubst %.fig,%.pdftex_t,$(FIGS))

SVGS       = $(wildcard svgs/*.svg)
SVGS_PDF   = $(patsubst %.svg,%.pdf,$(SVGS))

ODGS       = $(wildcard odgs/*.odg)
ODGS_PDF   = $(patsubst %.odg,%.pdf,$(ODGS))

SIMS_MAT 	= $(wildcard simus/*.m)
SIMS_MAT_PDF= $(patsubst %.m,%.pdf,$(SIMS_MAT))

NEEDED = $(FIGS_PDF) $(FIGS_PDF_T) $(SVGS_PDF) $(ODGS_PDF) $(SIMS_MAT_PDF)

export TEXINPUTS := ./texinputs/:$(TEXINPUTS)
export BIBINPUTS := ./bibinputs/:$(BIBINPUTS)
export BSTINPUTS := ./bibinputs/:$(BSTINPUTS)

#.SECONDARY: $(FIGS_PDF)

.PHONY: all clean dep

all: $(TOP)

$(TOP) : dep

dep: $(NEEDED) $(NEEDED_DOC)

%.pdf:%.tex $(STYS) bibsample.bib
	@echo "LaTeX search path $(TEXINPUTS)"
	@latexmk -pdf $<

clean: 
	latexmk -c $(TOP_NAME)

cleaner: 
	@echo Cleaning $(TOP) and $(NEEDED)
	@latexmk -C $(TOP_NAME)
	@rm -f $(NEEDED)


%.pdftex:%.fig
	fig2dev -L pdftex $< $@
%.pdftex_t:%.pdftex
	fig2dev -L pdftex_t -p $< $(patsubst %.pdftex_t,%.fig,$@) $@

%.swf:%.pdf
	pdf2swf $< && chmod -x $@

# Get Inkscape version
INKSCAPEVERSION = $(shell inkscape --version | cut -f2 -d" ")
# inkscapever -> x.yy.z
INKSCAPEVERSION_MAJOR = $(shell echo $(INKSCAPEVERSION) | cut -f1 -d.)

%.pdf:%.svg
	if [ "$(INKSCAPEVERSION_MAJOR)" = "0" ]; \
	then \
		inkscape -f $< --export-pdf=$@ ; \
	else \
		inkscape --export-type=pdf -o $@ $< ; \
	fi

%.pdf:%.odg
	libreoffice --headless --convert-to pdf $< --outdir odgs
	pdfcrop --margins 1 $@ $@

# %.pdf:%.m
# 	matlab -batch "addpath(genpath('simus')); $(notdir $*) "
# 	mv $(notdir $@) $@

%.pdf:%.m
	cd $(dir $<) && octave $(notdir $<)

# out.o: src.c src.h
#   $@   # "out.o" (target)
#   $<   # "src.c" (first prerequisite)
#   $^   # "src.c src.h" (all prerequisites)

# %.o: %.c
#   $*   # the 'stem' with which an implicit rule matches ("foo" in "foo.c")

documentation: IEEEtran_HOWTO/New_IEEEtran_how-to.tex
	cd IEEEtran_HOWTO && TEXINPUTS=../pngs/:../texinputs/:$$TEXINPUTS latexmk -pdf $(notdir $<)

preparation: paper_preparation/How-to-write-and-read-papers.tex
	cd paper_preparation && latexmk -pdf $(notdir $<)

clean-doc:
	cd IEEEtran_HOWTO && latexmk -c

clean-prep: 
	cd paper_preparation && latexmk -c 
